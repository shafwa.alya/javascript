
// Nomor 1

var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];

daftarHewan.sort()

console.log(daftarHewan)


// Nomor 2

function introduce({name, age, address, hobby}) {
    return "Nama saya " + name + "," + " umur saya " + age + " tahun, " + " alamat saya di " + address + "," + " dan saya punya hobby yaitu " + hobby
}

var data = {name : "John", age : 30, address : "Jalan Pelesiran", hobby : "gaming"}
var perkenalan = introduce(data)

console.log(perkenalan)

// Nomor 3
var vokal = ["a", "i", "u", "e", "o"]

function hitung_huruf_vokal(str) {
    var count = 0;

    for (var letter of Str.toLowerCase()) {
        if (vokal.includes(letter)) {
            count++;
        }
    }

    return count
}

var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1 , hitung_2) 

// Nomor 4

function hitung(x) {
    return (x*2)-2
}
console.log( hitung(0) ) // -2
console.log( hitung(1) ) // 0
console.log( hitung(2) ) // 2
console.log( hitung(3) ) // 4
console.log( hitung(5) ) // 8